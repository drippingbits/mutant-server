Mutant Server
=========

Mutant Server is a back-end for use with e.g. the Drippingbits Mutant UI.
Haskell is used to create a websockets interface handling and serving information from a database to the clients, whether they are players or game masters.
